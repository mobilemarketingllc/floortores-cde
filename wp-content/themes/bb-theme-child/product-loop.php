<div class="product-grid swatch" itemscope itemtype="http://schema.org/ItemList">

    <div class="row product-row">
    <?php 

    $show_financing = get_option('sh_get_finance');
    $col_class = 'col-md-3 col-sm-6 col-xs-12';
    if(get_option('salesbrand')!=''){
        $slide_brands = rtrim(get_option('salesbrand'), ",");
        $brandonsale = array_filter(explode(",",$slide_brands));
        $brandonsale = array_map('trim', $brandonsale);
    }
    $K = 1;

while ( have_posts() ): the_post(); 
      //collection field
      $collection = get_field('collection', $post->ID);
      $brand =  get_field('brand', $post->ID);
?>
    <div class="<?php echo $col_class; ?>">    
    <div class="fl-post-grid-post" itemprop="itemListElement" itemscope  itemtype="http://schema.org/ListItem">

    <meta itemprop="position" content="<?php echo $K;?>" />
        <?php // FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('swatch_image_link')) { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                  <?php 
                  
                   $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
							
							
					?>
            <img class="list-pro-image" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
            
            <?php
            // exclusive icon condition
            if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall' ||  $collection == 'Floorte Magnificent') {    ?>
			<span class="exlusive-badge"><img src="<?php echo plugins_url( '/product-listing-templates/images/exclusive-icon.png', dirname(__FILE__) );?>" alt="<?php the_title(); ?>" /></span>
			<?php } ?>
                  
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">
            <h4><?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { ?> <?php the_field('collection'); ?> <?php the_field('style'); ?> <?php } else{ ?><?php the_field('collection'); ?> <?php } ?> </h4>
            <h2 class="fl-post-grid-title" itemprop="name">
                <a href="<?php the_permalink(); ?>" itemprop="url" title="<?php the_title_attribute(); ?>"><?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { echo get_field('color'); } else{ echo get_field('color'); }?></a>
            </h2>

           <?php
           //var_dump(in_array(sanitize_title($brand),$brandonsale),sanitize_title($brand),$brandonsale);
           if(( get_option('getcouponbtn') == 1 && get_option('salesbrand')=='') || (get_option('getcouponbtn') == 1 && in_array(sanitize_title($brand),$brandonsale))){?>
                <a href="<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplaceurl');}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="fl-button getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
                <span class="fl-button-text"><?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplacetext');}else{ echo 'GET COUPON'; }?></span>
            </a>
            </a><br />
            <?php } ?>
           <?php if($show_financing == 1){?>
            <a href="<?php if(get_option('getfinancereplace')==1){ echo get_option('getfinancereplaceurl');}else{ echo '/flooring-financing/'; } ?>" target="_self" class="fl-button plp-getfinance-btn" role="button" >
                <span class="fl-button-text"><?php if(get_option('getfinancereplace')=='1'){ echo get_option('getfinancetext');}else{ echo 'Get Financing'; } ?></span>
            </a>
            <br />
           <?php } ?>

           
          
            <a class="link plp-view-product" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
        </div>
    </div>
    </div>
<?php  $K++; endwhile; ?>
</div>
</div>