		<?php do_action('fl_content_close'); ?>
	
	</div><!-- .fl-page-content -->
	<?php 
		
	do_action('fl_after_content'); 
	
	if ( FLTheme::has_footer() ) :
	
	?>
	<footer class="fl-page-footer-wrap" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
        <?php echo do_shortcode( "[fl_builder_insert_layout slug='footer5col' type='fl-builder-template']" ); ?>
		<?php

		do_action('fl_footer_wrap_open');
		do_action('fl_before_footer_widgets');

		FLTheme::footer_widgets();

		do_action('fl_after_footer_widgets');
		do_action('fl_before_footer');

		FLTheme::footer();

		do_action('fl_after_footer');
		do_action('fl_footer_wrap_close');
		?>
<div style="text-align: center; font-size: 12px; background-color: #fff; padding: 15px; margin: 0;">
    <copyright>&copy;<?php echo date('Y'); ?> The Floor Store. All Rights Reserved.
</copyright>
</div>
        <div style="text-align: center; font-size: 12px; background-color: #fff; padding: 15px; margin: 0;">
            <a target="_blank" href="http://www.diamondcertified.org/the-certification"><img class="foot-diamond" src="<?php echo get_bloginfo('url') ?>
/wp-content/uploads/2016/12/DC_blue_notag_.75-1.jpg" /></a>
        </div>
	</footer>
	<?php endif; ?>
	<?php do_action('fl_page_close'); ?>
</div><!-- .fl-page -->
<?php 
	
wp_footer(); 
do_action('fl_body_close');

FLTheme::footer_code();

?>
<script src="/wp-content/themes/bb-theme-child/resources/review_script.js"></script>
<div class="add_this_socials">
	<?php //echo do_shortcode('[addthis tool=addthis_vertical_follow_toolbox]');?>
</div>
<script src="https://calculator.measuresquare.com/scripts/jquery-m2FlooringCalculator.js"></script>

<script>
	$(function () {
		$('#calculateBtn').m2Calculator({
			measureSystem: "Imperial",            
			thirdPartyName: "The Floor Store", 
			thirdPartyEmail: "devteam@mobile-marketing.agency",  // if showDiagram = false, will send estimate data to this email when user click Email Estimate button
			showCutSheet: false, // if false, will not include cutsheet section in return image
			showDiagram: true,  // if false, will close the popup directly 
		  /*  product: {
				type: "Carpet",
				name: "Carpet 1",
				width: "6'0\"",
				length: "150'0\"",
				horiRepeat: "3'0\"",
				vertRepeat: "3'0\"",
				horiDrop: "",
				vertDrop: ""
			},
			*/
			cancel: function () {
				//when user closes the popup without calculation.
			},
			callback: function (data) {
				//json format, include user input, usage and base64image
				$("#callback").html(JSON.stringify(data));   
				console.log(data.input)
				$("#usageText").val(data.usage);    
				$("#image").attr("src", data.img);  //base64Image 
				window.location.href = "https://www.floorstores.com/thank-you-contact-us/";
			}
		});
	});
 </script>
</body>
</html>