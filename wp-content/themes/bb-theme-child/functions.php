<?php
//session_start();


// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js",array("jquery"));
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
  wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
});


//add sitemap menu
function register_my_menu() {
    register_nav_menu('sitemap',__( 'Sitemap' ));
}
add_action( 'init', 'register_my_menu' );



// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}
add_filter('wpseo_metabox_prio', 'yoasttobottom');

/**
 * URL Shortcode
 * @author Bill Erickson
 * @link http://www.billerickson.net/wordpress-shortcode-sidebar-widget/
 * @return Site URL.
 */
function url_shortcode() {
    return get_bloginfo('url');
}
add_shortcode('url','url_shortcode');

// add description in mega menu
class Menu_Subs extends Walker_Nav_Menu {
    //function start_el(&$output, $item, $depth, $args) {
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="' . esc_attr( $class_names ) . '"';

        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

        $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
        $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
        $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '<br /><span class="sub">' . $item->description . '</span>';
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

function add_header_menu_walker( $args ) {
    if($args['theme_location'] == 'header') {
        return array_merge( $args, array(
            'walker' => new Menu_Subs
        ) );
    }
    return $args;
}
add_filter( 'wp_nav_menu_args', 'add_header_menu_walker' );


// Stop WP removing HTML in Menu description area

remove_filter('nav_menu_description', 'strip_tags');
add_filter( 'wp_setup_nav_menu_item', 'cus_wp_setup_nav_menu_item' );
function cus_wp_setup_nav_menu_item($menu_item) {
    $menu_item->description = apply_filters('nav_menu_description',  $menu_item->post_content );
    return $menu_item;
}

//custom facetwp result count
function my_facetwp_result_count( $output, $params ) {
    //$output = $params['lower'] . '-' . $params['upper'] . ' of ' . $params['total'] . ' results';
    $output  = ' Viewing ' . $params['total'] . ' Products';
    return $output;
}
add_filter( 'facetwp_result_count', 'my_facetwp_result_count', 10, 2 );

// filter the Gravity Forms button type
add_filter( 'gform_submit_button_52', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button class='button' onclick='return submitForm()' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}

// filter the Gravity Forms button type - Karastan Coupon Landing Page
add_filter( 'gform_submit_button_41', 'form_submit_button_karastan', 10, 2 );
function form_submit_button_karastan( $button, $form ) {
    return "<button class='button' onclick='return submitForm()' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}

//filter to add hidden date and timestamp
add_filter( 'gform_field_input', 'tracker', 10, 5 );
function tracker( $input, $field, $value, $lead_id, $form_id ) {
    // because this will fire for every form/field, only do it when it is the specific form and field
     if ( $field->cssClass == 'mf_date_of_submission' ) 
     {
        $input = '<input type="hidden" id="hidTracker" name="hidTracker" value="'.date("m/d/Y h:i:s A").'">';
     }
    return $input;
}

// Action to hide fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    
   ?>
      <style>
          .mf_formUsed ,.mf_date_of_submission {
             display: none !important;                
           }
           body .gform_wrapper ul li.field_description_below.mf_productCategory div.ginput_container_checkbox {
    margin-top: 10px !important;
}
      </style>
   
   <?php
    
}

function new_year_number() 
{
	return $new_year = date('Y');
}
add_shortcode('year_code_4', 'new_year_number');

function new_year_number_2() 
{
	return $new_year = date('y');
}
add_shortcode('year_code_2', 'new_year_number_2');

wp_clear_scheduled_hook( 'roomvo_csv_integration_cronjob' );

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail',90 );

function wpse_override_yoast_breadcrumb_trail( $links ) {
    if (is_singular( 'carpeting' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring Products',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpeting/',
            'text' => 'Carpeting',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpeting/browse-carpeting-catalog/',
            'text' => 'Browse Carpeting Catalog',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }
    else if (is_singular( 'hardwood_catalog' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring Products',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood-flooring/',
            'text' => 'Hardwood Floors',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood-flooring/browse-hardwood-catalog/',
            'text' => 'Browse Hardwood Catalog',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }
    else if (is_singular( 'laminate_catalog' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring Products',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/',
            'text' => 'Laminate Floors',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/browse-laminate-flooring-catalog/',
            'text' => 'Browse Laminate Flooring Catalog',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }
    else if (is_singular( 'solid_wpc_waterproof' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring Products',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof-sheet/',
            'text' => 'Waterproof Sheet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof-sheet/browse-waterproof-flooring-catalog/',
            'text' => 'Browse Waterproof Flooring Catalog',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }
    else if (is_singular( 'luxury_vinyl_tile' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring Products',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl-sheet/',
            'text' => 'Luxury Vinyl Tile | Vinyl Floors',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl-sheet/browse-vinyl-flooring-catalog/',
            'text' => 'Browse Vinyl Flooring Catalog',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }
    else if (is_singular( 'tile_catalog' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring Products',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/ceramic-tile-floors/',
            'text' => 'Ceramic Tile Floors',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/ceramic-tile-floors/browse-ceramic-tile-flooring-catalog/',
            'text' => 'Browse Ceramic Tile Flooring Catalog',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }

    return $links;
}



add_filter( 'gform_notification_57', 'customer_responder_57', 10, 3 );

function customer_responder_57( $notification, $form, $entry ) {
    if ( $notification['name'] == 'Autoresponder Coupon' ) {
        $notification['message'] ="";
        $promoCodeId = "";

        foreach ( $form['fields'] as $field ) {
            
            if( $field->label == 'promoCode' ) {
                $promoCodeId = $field->id;
            }
         }
         $promoCode = "";
        if($promoCodeId != ""){
            $promoCode = rgar( $entry, $promoCodeId );
        }
         
         
        if($promoCode !=""){
            $saleinformation = json_decode(get_option('saleinformation'));

            $saleinformation = (array) $saleinformation;
            $saleinformation = (array) $saleinformation[$promoCode];
            if(count($saleinformation) > 0) {
                $image_onform = $saleinformation['image_onform'];
                $banner_img_deskop = $saleinformation['banner_img_deskop'];
                $content = $saleinformation['content'];
                $endDate = date("m/d/Y", strtotime($saleinformation['endDate']));
                $sale_name = $saleinformation['sale_name'];
    
    
                $notification['message'] .= '<!-- HEADER -->
            <table style="width: 100%; background-color: #fff; margin: 0px; height: 80px;">
            <tbody>
            <tr>
            <td></td>
            <td style="display: block; max-width: 600px; margin: 0 auto; clear: both; padding: 0px;">
            <div style="display: block; max-width: 600px; margin: 0 auto; clear: both;">
            <table style="padding: 0px 0px 0px 0px; max-width: 600px; margin: 0 auto; display: block;">
            <tbody>
            <tr>
            <td style="width: 300px;"><img src="https://www.floorstores.com/wp-content/uploads/2017/01/thefloorstore_logo-diamondCert.png" /></td>
            <td style="width: 300px; text-align: right; font-size: 16px; font-family: \'Montserrat\', sans-serif; line-height: 1.1; margin-bottom: 5px; color: #000;">(866) FLOORSTORES</td>
            </tr>
            </tbody>
            </table>
            </div></td>
            <td></td>
            </tr>
            </tbody>
            </table>
            <!-- /HEADER --><!-- BODY -->
            <table style="display: block; max-width: 600px; margin: 0 auto; clear: both;">
            <tbody>
            <tr>
            <td></td>
            <td style="display: block; max-width: 600px; margin: 0 auto; clear: both;" bgcolor="#f5f4f3">
            <div style="display: block; max-width: 600px; margin: 0 auto; clear: both; padding: 0px;">
            <table style="display: block; max-width: 600px; margin: 0 auto; clear: both; padding: 0px;" border="0px">
            <tbody>
            <tr>
            <td><img title="'.$sale_name.'" src="'.$banner_img_deskop.'" width="600" />
            <p style="font-family: \'Montserrat\', sans-serif; text-align: left; font-weight: 400; font-size: 11px; color: #6a6c6c;">Dear {Your Name:1},</p>
            Here is the coupon you requested on <a style="color: #d93600; text-decoration: underline; font-family: \'Montserrat\', sans-serif;" href="https://www.floorstores.com">www.floorstores.com</a>. Please print this email and present it to the sales associate at the time of purchase.<!-- // Begin Module: Coupon \\ -->
            <table style="background-color: #df984a; padding: 10px;" border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
            <td style="background-color: #df984a:;">
            <table style="border: 2px dotted #fff; padding: 10px; background-color: #df984a;" border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
            <td style="background-color: #df984a;" colspan="2" align="center">
            <h2 style="font-size: 12px; text-align: center; font-weight: normal; width: 200px; padding-bottom: 10px; color: #fff; padding-top: 10px; font-family: \'Montserrat\', sans-serif; background-color: #df984a;">OFFER EXPIRES: '.$endDate.'</h2>
            </td>
            </tr>
            <tr>
            <td style="border-right: 1px #ffffff solid; text-align: center; padding: 0px;" align="center" valign="middle" width="300"><img title="'.$sale_name.'" width="280" src="'.$image_onform.'" /></td>
            <td style="padding: 8px;" align="center" valign="middle">
            <p style="font-size: 12px; line-height: 18px; font-family: \'Montserrat\', sans-serif; margin-bottom: 0px; color: #ffffff; padding-bottom: 6px;">Redeemable at:<b> <span style="color: #ffffff; font-weight: bold; font-size: 15px;">The Floor Store Location</span></b> <span style="color: #fff;">{Preferred Location:5}</span></p>
            <p style="font-size: 12px; line-height: 18px; font-family: \'Montserrat\', sans-serif; font-weight: 100; margin-top: 0px; padding-top: 10px; color: #ffffff;">COUPON CODE:<b>$500OFF</b><br> {Your Name:1} <br> {Your Email:2}<br> {Mobile Number:3}<br> {date_mdy}</p>
            </td>
            </tr>
            <tr>
            <td style="text-align: center; padding: 4px; padding-top: 30px; color: #ffffff;" colspan="2" align="center" valign="middle">
            <p style="color: #fffff; font-size: 10px; line-height: 14px; font-family: \'Montserrat\', sans-serif;">'.$content.'</p>
            </td>
            </tr>
            </tbody>
            </table>
            </td>
            </tr>
            </tbody>
            </table>
            <!-- Callout Panel -->
            <p style="font-family: \'Montserrat\', sans-serif; font-weight: 100; text-align: center;"><img title="Thank you - From your friends at The Floor Store" src="https://www.floorstores.com/wp-content/uploads/2015/06/thankYou.jpg" /></p>
            <p style="font-family: \'Montserrat\', sans-serif; font-size: 11px; color: #6a6c6c; text-align: center;">If you have any questions or concerns contact our customer service department at <a style="color: #d93600; text-decoration: underline;" href="https://www.floorstores.com/contact-us/" target="_blank" rel="noopener noreferrer">The Floor Store</a></p>
            <p style="text-align: center;"><a title="The Floor Store - Facebook" href="https://www.facebook.com/pages/The-Floor-Store/158719140819812" target="_blank" rel="noopener noreferrer"><img src="https://www.floorstores.com/wp-content/uploads/2015/06/fb_icon.jpg" /></a> <a title="The Floor Store - YouTube" href="https://www.youtube.com/user/TheFloorStoreCA" target="_blank" rel="noopener noreferrer"><img src="https://www.floorstores.com/wp-content/uploads/2015/06/yt_icon.jpg" /></a> <a title="The Floor Store - Twitter" href="https://twitter.com/the_floor_store" target="_blank" rel="noopener noreferrer"><img src="https://www.floorstores.com/wp-content/uploads/2015/06/twitter_icon.jpg" /></a></p>
            <p style="font-family: \'Montserrat\', sans-serif; font-weight: 100; text-align: center;"><!-- social & contact --> <!-- /BODY --> <!-- FOOTER --></p>
            
            <table style="display: block; max-width: 600px; margin: 0 auto; clear: both; font-family: \'Montserrat\', sans-serif; font-size: 12px; color: #666;">
            <tbody>
            <tr>
            <td></td>
            <td style="display: block; max-width: 600px; margin: 0 auto; clear: both;"><!-- content -->
            <div style="display: block; max-width: 600px; margin: 0 auto; clear: both;">
            <table>
            <tbody>
            <tr>
            <td style="text-align: center; font-family: \'Montserrat\', sans-serif; font-weight: 100; font-size: 10px; color: #6a6c6c; padding-top: 10px; padding-bottom: 20px;">© 2020 The Floor Store. (866) FLOORSTORES
            <a style="color: #d93600;" href="https://www.floorstores.com/locations/flooring-richmond-ca/" target="_blank" rel="noopener noreferrer">ALBANY, CA</a> | <a style="color: #d93600;" href="https://www.floorstores.com/locations/flooring-concord-ca/" target="_blank" rel="noopener noreferrer">CONCORD, CA </a> |<a style="color: #d93600;" href="https://www.floorstores.com/locations/flooring-dublin-ca/" target="_blank" rel="noopener noreferrer">DUBLIN, CA</a> | <a style="color: #d93600;" href="https://www.floorstores.com/locations/flooring-san-francisco-ca/" target="_blank" rel="noopener noreferrer">SAN FRANCISCO, CA</a> | <a style="color: #d93600;" href="https://www.floorstores.com/locations/flooring-san-jose-ca/" target="_blank" rel="noopener noreferrer">SAN JOSE, CA</a> |<a style="color: #d93600;" href="https://www.floorstores.com/locations/flooring-san-rafael-ca/" target="_blank" rel="noopener noreferrer">SAN RAFAEL, CA</a> | <a style="color: #d93600;" href="https://www.floorstores.com/locations/flooring-santa-rosa-ca/" target="_blank" rel="noopener noreferrer">SANTA ROSA, CA</a> | <a style="color: #d93600;" href="https://www.floorstores.com/locations/flooring-sunnyvale-ca/" target="_blank" rel="noopener noreferrer">SUNNYVALE, CA | </a><a style="color: #d93600;" href="https://www.floorstores.com/locations/flooring-san-carlos-ca/" target="_blank" rel="noopener noreferrer">SAN CARLOS, CA</a></td>
            </tr>
            </tbody>
            </table>
            </div>
            <!-- /content --></td>
            <td></td>
            </tr>
            </tbody>
            </table>
            <p style="font-family: \'Montserrat\', sans-serif; font-weight: 100; text-align: center;"><!-- /FOOTER --></p>
            </td>
            </tr>
            </tbody>
            </table>
            </div></td>
            </tr>
            </tbody>
            </table>';
            }
           

        }
        
    }
 

 return $notification;
}