<?php
//SEND THE SETTINGS FROM THE CUSTOM BEAVER BUILDER MODULE
$settings=$_SESSION["module_settings"];
$module=$_SESSION["module_info"];


global $wp_query;
$query=$wp_query;

// Render the posts.
if($query->have_posts()) :
?>
<div id="main" class="fl-post-<?php echo $settings->layout; ?> faceted_results" itemscope="itemscope" itemtype="http://schema.org/Blog">
    <?php
    while($query->have_posts()){
        $query->the_post(); 
        include $module["dir"] . 'includes/post-' . $settings->layout . '.php'; 
    }
    ?>
    <div class="fl-post-grid-sizer"></div>
</div>
<div class="fl-clear"></div>
<?php endif; ?>
<?php

// Render the pagination.
if($settings->pagination != 'none' && $query->have_posts()) :
    /*
    ?>
    <div class="pagination fl-builder-pagination"<?php if($settings->pagination == 'scroll') echo ' style="display:none;"'; ?>>
        <?php 
        // global $wp_query;
        // $oquery=$wp_query;
        // $wp_query=$query;
        // FLBuilderLoop::pagination($query);
        // $wp_query=$oquery;
         ?>
    </div>
    <?php */echo do_shortcode('[facetwp pager="true"]') ?>

<?php endif; ?>



<?php

// Render the empty message.
if(!$query->have_posts() && (defined('DOING_AJAX') || isset($_REQUEST['fl_builder']))) :

?>
<div class="fl-post-grid-empty">
    <?php 
    if (isset($settings->no_results_message)) :
        echo $settings->no_results_message;
    else :
        _e( 'No posts found.', 'fl-builder' );
    endif; 
    ?>
</div>
    
<?php

endif;

wp_reset_postdata();
